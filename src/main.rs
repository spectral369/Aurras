use std::{
    fs::{self, read_to_string, File, OpenOptions},
    io::{BufRead, BufReader, Write},
    path::Path,
    process::{self, exit, Command, Stdio},
    sync::Arc,
    time::{Duration, Instant},
};

use async_trait::async_trait;
use futures::{lock::Mutex, StreamExt};
use regex::Regex;
use songbird::{
    input::{ChildContainer, Compose, YoutubeDl},
    shards::TwilightMap,
    tracks::{PlayMode, TrackHandle},
    EventContext, EventHandler, Songbird,
};
use std::{collections::HashMap, error::Error, future::Future, num::NonZeroU64};
use tokio::sync::RwLock;
use twilight_cache_inmemory::{InMemoryCache, ResourceType};
use twilight_gateway::{
    stream::{self, ShardEventStream},
    Event, EventType, Intents, Shard,
};
use twilight_http::Client as HttpClient;
use twilight_model::{
    channel::Message,
    gateway::{
        payload::outgoing::UpdatePresence,
        presence::{Activity, ActivityType, MinimalActivity, Status},
    },
    id::{marker::GuildMarker, Id},
};
use twilight_standby::Standby;
use twilight_util::builder::embed::{EmbedBuilder, EmbedFieldBuilder, ImageSource};

mod yt_utils;

type State = Arc<StateRef>;

#[derive(Debug)]
struct StateRef {
    http: HttpClient,
    trackdata: RwLock<HashMap<Id<GuildMarker>, TrackHandle>>,
    songbird: Songbird,
    sender: Arc<TwilightMap>,
    shardid: u64,
    standby: Standby,
    cache: InMemoryCache,
}

struct Queue1 {
    queue: Vec<YoutubeDl>,
}

#[async_trait]
impl EventHandler for Queue1 {
    async fn act(&self, _ctx: &EventContext<'_>) -> Option<songbird::Event> {
        if !self.queue.is_empty() {
            let mut _src = self.queue[0].clone();
        }

        println!("song finished ");
        return None;
    }
}

impl Queue1 {
    pub fn remove_fist(&mut self) {
        if self.queue.len() > 0 {
            self.queue.remove(0);
        }
    }
}

#[derive(Clone)]
struct StateInfo {
    is_joined: bool,
    current_song_desc: String,
    current_song_link: String,
    _yt_utils: yt_utils::YtInfo,
    current_song_length: Option<Duration>,
    is_playing: bool,
    ffmpeg_id: Vec<u32>,
}

impl StateInfo {
    pub fn set_is_joined(&mut self, value: bool) {
        self.is_joined = value;
    }
    pub fn set_current_song_desc(&mut self, value: String) {
        self.current_song_desc = value;
    }
    pub fn set_current_song_link(&mut self, value: String) {
        self.current_song_link = value;
    }
    pub fn set_current_song_length(&mut self, value: Option<Duration>) {
        self.current_song_length = value;
    }
    pub fn set_is_playing(&mut self, value: bool) {
        self.is_playing = value;
    }
    pub fn set_ffmpeg_id(&mut self, value: u32) {
        self.ffmpeg_id.push(value);
    }
}

fn spawn(
    fut: impl Future<Output = Result<(), Box<dyn Error + Send + Sync + 'static>>> + Send + 'static,
) {
    tokio::spawn(async move {
        if let Err(why) = fut.await {
            println!("{}", &why);
        }
    });
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let (mut shards, state, state_info, queue) = {
        let token = get_discord_token();
        if token.len() < 30 {
            println!("{:?} - {}", token, "Is not valid token !");
            process::exit(0x0100);
        }

        let http = HttpClient::new(token.clone());
        let user_id = http.current_user().await?.model().await?.id;

        let intents = Intents::GUILD_MESSAGES
            | Intents::DIRECT_MESSAGES
            | Intents::GUILD_MEMBERS
            | Intents::GUILDS
            | Intents::GUILD_PRESENCES
            | Intents::GUILD_VOICE_STATES
            | Intents::MESSAGE_CONTENT;
        let config = twilight_gateway::Config::new(token.clone(), intents);

        let shards: Vec<Shard> =
            stream::create_recommended(&http, config, |_, builder| builder.build())
                .await?
                .collect();

        let senders = TwilightMap::new(
            shards
                .iter()
                .map(|s| (s.id().number(), s.sender()))
                .collect(),
        );
        let mysender = Arc::new(senders);

        let senders2 = mysender.clone();

        let myshardid = shards.get(0).unwrap().id().number();

        let activity = Activity::from(MinimalActivity {
            kind: ActivityType::Listening,
            name: "Nothing".to_owned(),
            url: None,
        });
        let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;
        let _res = mysender.get(myshardid).unwrap().command(&request);

        let songbird = Songbird::twilight(senders2, user_id);

        (
            shards,
            Arc::new(StateRef {
                http,
                trackdata: Default::default(),
                songbird,
                sender: mysender,
                shardid: myshardid,
                standby: Standby::new(),
                cache: InMemoryCache::builder()
                    .resource_types(ResourceType::VOICE_STATE | ResourceType::GUILD)
                    .build(),
            }),
            Arc::new(Mutex::new(StateInfo {
                is_joined: false,
                current_song_desc: String::default(),
                current_song_link: String::default(),
                _yt_utils: Default::default(),
                current_song_length: Option::default(),
                is_playing: false,
                ffmpeg_id: Vec::default(),
            })),
            Arc::new(Mutex::new(Queue1 {
                queue: Vec::default(),
            })),
        )
    };

    let mut stream = ShardEventStream::new(shards.iter_mut());
    loop {
        let event = match stream.next().await {
            Some((_, Ok(event))) => event,
            Some((_, Err(source))) => {
                println!("Error");

                if source.is_fatal() {
                    break;
                }

                continue;
            }
            None => break,
        };

        state.standby.process(&event);
        state.cache.update(&event);
        state.songbird.process(&event).await;

        if (event.kind() == EventType::VoiceStateUpdate)
            || (event.kind() == EventType::VoiceServerUpdate)
        {
            let user_id2 = state.http.current_user().await?.model().await?.id;

            let voice_id = state.cache.voice_state(user_id2, event.guild_id().unwrap());
            let testss;
            match voice_id {
                Some(test01) => {
                    testss = Some(test01.channel_id());
                }
                None => {
                    testss = None;
                }
            }
            if testss.is_some() {
                let info = state
                    .cache
                    .voice_channel_states(testss.unwrap())
                    .unwrap()
                    .count();
                if info < 2 {
                    if !state_info.lock().await.ffmpeg_id.is_empty() {
                        let id = state_info.lock().await.ffmpeg_id.pop().unwrap();
                        let sp = Command::new("kill").arg(id.to_string()).output();
                        let _res = sp.unwrap();
                    }

                    if let Some(call_lock) = state.songbird.get(event.guild_id().unwrap()) {
                        let mut call = call_lock.lock().await;
                        let _ = call.stop();
                        state_info.lock().await.set_is_playing(false);
                    }

                    let _st1 = state.songbird.leave(event.guild_id().unwrap());
                    let _st2 = state.songbird.remove(event.guild_id().unwrap());
                    println!("{:?}", _st1.await);
                    println!("{:?}", _st2.await);

                    state_info.lock().await.set_is_joined(false);

                    let activity = Activity::from(MinimalActivity {
                        kind: ActivityType::Listening,
                        name: "Nothing".to_owned(),
                        url: None,
                    });
                    let request =
                        UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

                    let _res = state.sender.get(state.shardid).unwrap().command(&request);
                }
            }
        }
        if let Event::MessageCreate(msg) = event {
            if msg.guild_id.is_none() || !msg.content.starts_with('!') {
                continue;
            }

            match msg.content.splitn(2, ' ').next() {
                Some("!join") => spawn(join(msg.0, Arc::clone(&state), Arc::clone(&state_info))),
                Some("!leave") => {
                    spawn(leave(msg.0, Arc::clone(&state), Arc::clone(&state_info)));
                }
                Some("!play") => spawn(play(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                    Arc::clone(&queue),
                )),
                Some("!pause") => spawn(pause(msg.0, Arc::clone(&state), Arc::clone(&state_info))),
                Some("!help") => spawn(help(msg.0, Arc::clone(&state), Arc::clone(&state_info))),
                Some("!radiolist") => spawn(radiolist(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!stop") => spawn(stop(msg.0, Arc::clone(&state), Arc::clone(&state_info))),
                Some("!time") => spawn(time(msg.0, Arc::clone(&state), Arc::clone(&state_info))),
                Some("!add") => spawn(add(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                    Arc::clone(&queue),
                )),
                Some("!list") => spawn(list(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                    Arc::clone(&queue),
                )),
                Some("!desc") => spawn(description(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!radiozu") => {
                    spawn(radiozu(msg.0, Arc::clone(&state), Arc::clone(&state_info)))
                }
                Some("!radiovirgin") => spawn(radiovirgin(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!radio24house") => spawn(radio24house(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!radiouv") => {
                    spawn(radiouv(msg.0, Arc::clone(&state), Arc::clone(&state_info)))
                }
                Some("!radiohouse") => spawn(radiohouse(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!radioroman") => spawn(radioroman(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!radiodancefm") => spawn(radiodancefmro(
                    msg.0,
                    Arc::clone(&state),
                    Arc::clone(&state_info),
                )),
                Some("!volume") => {
                    spawn(volume(msg.0, Arc::clone(&state), Arc::clone(&state_info)))
                }
                Some("!repeat") => spawn(time(msg.0, Arc::clone(&state), Arc::clone(&state_info))),
                Some("!reload") => {
                    exit(3);
                }

                _ => continue,
            }
        }
    }

    Ok(())
}

async fn join(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let guild_id = msg.guild_id.ok_or("Can't join a non-guild channel.")?;
    let user_id = msg.author.id;

    let channel_to_join: Option<u64>;
    let user_ch = state.cache.voice_state(user_id, guild_id.clone());
    match user_ch {
        Some(test01) => {
            channel_to_join = Some(test01.channel_id().get());
        }
        None => {
            state
                .http
                .create_message(msg.channel_id)
                .content("You're not in a voice channel?")?
                .await?;
            return Ok(());
        }
    }
    let channel_id = NonZeroU64::new(channel_to_join.unwrap())
        .ok_or("Joined voice channel must have nonzero ID.")?;
    let content = match state.songbird.join(guild_id, channel_id).await {
        Ok(_handle) => {
            state_info.lock().await.set_is_joined(true);
            format!("Joined <#{}>!", channel_id)
        }
        Err(e) => format!("Failed to join <#{}>! Why: {:?}", channel_id, e),
    };

    state
        .http
        .create_message(msg.channel_id)
        .content(&content)?
        .await?;

    Ok(())
}

async fn leave(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let guild_id = msg.guild_id.unwrap();

    if state_info.lock().await.is_joined {
        if let Some(call_lock) = state.songbird.get(guild_id) {
            let mut call = call_lock.lock().await;
            let _ = call.stop();
            state_info.lock().await.set_is_playing(false);
        }
        let activity = Activity::from(MinimalActivity {
            kind: ActivityType::Listening,
            name: "Nothing".to_string(),
            url: None,
        });
        let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

        let _res = state.sender.get(state.shardid).unwrap().command(&request);

        state.songbird.leave(guild_id).await?;
        state_info.lock().await.set_is_joined(false);
        state.songbird.remove(guild_id).await?;

        state
            .http
            .create_message(msg.channel_id)
            .content("Left the channel")?
            .await?;
    } else {
        state
            .http
            .create_message(msg.channel_id)
            .content("Not in a channel")?
            .await?;
    }

    Ok(())
}

async fn play(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
    queue: Arc<Mutex<Queue1>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let now = Instant::now();
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => {
                println!("{:?}", result)
            }
            None => println!("ERR"),
        }
    }

    if state_info.lock().await.is_joined {
        let b = msg.content.clone();
        let index1 = b.find(" ");
        let mut text: String;
        if !index1.is_none() {
            text = b.chars().skip(index1.unwrap()).collect();
        } else {
            text = "".to_string();
        }
        text = text.trim().replace(" ", "+");

        let re = Regex::new(r"^(http(s)://)?((w){3}.)?youtu(be|.be)?(.com)?/.+").unwrap();
        let re2 = Regex::new("^(http://)(.+)").unwrap();
        let mut yt_link: String = String::from("https://www.youtube.com/watch?v=");
        if re.is_match(&text) | re2.is_match(&text) {
            yt_link = text.to_string().to_string();
        } else if text.len() < 1 {
            yt_link = String::from("http://astreaming.virginradio.ro:8000/virgin_aacp_64k");
        } else {
            let mut search_str: String =
                String::from("https://www.youtube.com/results?search_query=");
            search_str.push_str(&text);

            let content = reqwest::get(search_str.to_string()).await?.text().await?;
            yt_link.push_str(
                &yt_utils::_extract_links(content.as_str())
                    .iter()
                    //  .skip(1)
                    .next()
                    .unwrap()
                    .to_string(),
            );
            state_info
                .lock()
                .await
                .set_current_song_link(yt_link.clone());
        }

        let guild_id = msg.guild_id.unwrap();

        let mut que1 = queue.lock().await;
        let queue_list = que1.queue.clone();
        que1.remove_fist();

        //   let queue_list = &queue.lock().await.queue;
        let mut src;
        if !queue_list.is_empty() {
            src = queue_list[0].clone();
        } else {
            src = YoutubeDl::new(reqwest::Client::new(), yt_link);
        }

        if let Ok(metadata) = src.aux_metadata().await {
            let content = format!(
                "Playing **{:?}**",
                metadata.title.as_ref().unwrap_or(&"<UNKNOWN>".to_string()),
            );
            state_info
                .lock()
                .await
                .set_current_song_length(metadata.duration);

            state
                .http
                .create_message(msg.channel_id)
                .content(&content)?
                .await?;

            if let Some(call_lock) = state.songbird.get(guild_id) {
                if state_info.lock().await.is_playing {
                    let mut call = call_lock.lock().await;
                    let _ = call.stop();
                    state_info.lock().await.set_is_playing(false);
                }

                let mut call = call_lock.lock().await;
                let handle = call.play_input(src.into());
                state_info.lock().await.set_is_playing(true);

                let activity = Activity::from(MinimalActivity {
                    kind: ActivityType::Listening,
                    name: metadata.title.unwrap_or("<unknown>".to_string()),
                    url: None,
                });
                let request =
                    UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

                let _res = state.sender.get(state.shardid).unwrap().command(&request);

                let mut store = state.trackdata.write().await;
                store.insert(guild_id, handle);

                let gr = store.get_key_value(&guild_id);

                let y = gr.unwrap().1;

                let queue2 = &queue_list.clone();

                let queue = queue2.clone();

                let _res = y.add_event(
                    songbird::Event::Track(songbird::TrackEvent::End),
                    Queue1 { queue },
                );
            }
        } else {
            state
                .http
                .create_message(msg.channel_id)
                .content("Didn't find any results")?
                .await?;
        }
    }
    let elapsed = now.elapsed();
    println!("Elapsed Youtube: {:.2?}", elapsed);
    Ok(())
}

async fn pause(
    msg: Message,
    state: State,
    _state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let guild_id = msg.guild_id.unwrap();

    let store = state.trackdata.read().await;

    let content = if let Some(handle) = store.get(&guild_id) {
        let info = handle.get_info().await?;

        let paused = match info.playing {
            PlayMode::Play => {
                let _success = handle.pause();
                false
            }
            _ => {
                let _success = handle.play();
                true
            }
        };

        let action = if paused { "Unpaused" } else { "Paused" };

        format!("{} the track", action)
    } else {
        format!("No track to (un)pause!")
    };

    state
        .http
        .create_message(msg.channel_id)
        .content(&content)?
        .await?;

    Ok(())
}

async fn stop(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let guild_id = msg.guild_id.unwrap();

    if let Some(call_lock) = state.songbird.get(guild_id.into_nonzero()) {
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();
            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }
        if state_info.lock().await.is_playing {
            let mut call = call_lock.lock().await;
            let _ = call.stop();
            state_info.lock().await.set_is_playing(false);
        }
        let activity = Activity::from(MinimalActivity {
            kind: ActivityType::Listening,
            name: "Nothing".to_owned(),
            url: None,
        });
        let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

        let _res = state.sender.get(state.shardid).unwrap().command(&request);
    }

    state
        .http
        .create_message(msg.channel_id)
        .content("Stopped the track")?
        .await?;

    Ok(())
}

async fn volume(
    msg: Message,
    state: State,
    _state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let guild_id = msg.guild_id.unwrap();
    let mut content = msg.content.clone();
    content = content[7..].to_string().to_string();
    content.retain(|f| !f.is_whitespace());

    if content.to_string().is_empty() {
        state
            .http
            .create_message(msg.channel_id)
            .content("Use !volume <value>")?
            .await?;
    } else {
        let volume = content.parse::<f32>()?;

        if !volume.is_finite() || volume > 10.0 || volume < 0.0 {
            state
                .http
                .create_message(msg.channel_id)
                .content("Invalid volume!")?
                .await?;

            return Ok(());
        }

        let store = state.trackdata.read().await;

        let content = if let Some(handle) = store.get(&guild_id) {
            let _success = handle.set_volume(volume as f32);
            format!("Set the volume to {}", volume)
        } else {
            format!("No track to change volume!")
        };

        state
            .http
            .create_message(msg.channel_id)
            .content(&content)?
            .await?;
    }

    Ok(())
}

async fn help(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let path = fs::canonicalize("./help.txt");
        let file = File::open(path.unwrap()).expect("err");
        let reader = BufReader::new(file);

        let mut embed_builder = EmbedBuilder::new();
        embed_builder = embed_builder.description("Commands:");

        for (index, line) in reader.lines().enumerate() {
            let data = line.unwrap();

            let f1 = EmbedFieldBuilder::new(String::from(index.to_string()), data)
                .inline()
                .build();
            embed_builder = embed_builder.field(f1);
        }

        let embed = embed_builder.validate()?.build();
        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
    }

    Ok(())
}

async fn radiolist(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let path = fs::canonicalize("./radiolist.txt");
        let file = File::open(path.unwrap()).expect("err");
        let reader = BufReader::new(file);

        let mut embed_builder = EmbedBuilder::new();
        embed_builder = embed_builder.description("Radio List:");

        for (index, line) in reader.lines().enumerate() {
            let data = line.unwrap();

            let f1 = EmbedFieldBuilder::new(String::from(index.to_string()), data)
                .inline()
                .build();
            embed_builder = embed_builder.field(f1);
        }

        let embed = embed_builder.validate()?.build();
        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
    }

    Ok(())
}

async fn time(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let guild_id = msg.guild_id.unwrap();

        let store = state.trackdata.read().await;

        let content = if let Some(handle) = store.get(&guild_id) {
            let info = handle.get_info().await?;

            let time_elapsed = info.position;
            let time_elapsed_hours = (time_elapsed.as_secs() / 60) / 60;
            let time_elapsed_minutes = (time_elapsed.as_secs() / 60) % 60;
            let time_elapsed_seconds = time_elapsed.as_secs() % 60;
            let total_time = state_info
                .lock()
                .await
                .current_song_length
                .unwrap_or(Duration::default());
            let total_time_hours = (total_time.as_secs() / 60) / 60;
            let total_time_minutes = (total_time.as_secs() / 60) % 60;
            let total_time_seconds = total_time.as_secs() % 60;

            format!(
                "{time_elapsed_hours:.1}H:{time_elapsed_minutes:.1}m:{time_elapsed_seconds:.1}s/{total_time_hours:.1}H:{total_time_minutes:.1}m:{total_time_seconds:.1}s"
            )
        } else {
            format!("Error gettting duration")
        };
        let mut part1: String = "`".to_string().to_owned();
        part1.push_str(&content);
        part1.push_str("`");

        state
            .http
            .create_message(msg.channel_id)
            .content(&part1)?
            .await?;
    }

    Ok(())
}

async fn list(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
    queue: Arc<Mutex<Queue1>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    let mut counter: i16 = 0;
    if state_info.lock().await.is_joined {
        let list = &queue.lock().await.queue;

        if list.is_empty() {
            state
                .http
                .create_message(msg.channel_id)
                .content(&"No songs in queue!")?
                .await?;
        } else {
            for item in list {
                counter = counter + 1;
                let dat = item.clone().aux_metadata().await;
                let title = dat
                    .unwrap_or_default()
                    .title
                    .unwrap_or("UNKNOWN".to_string());

                let mut content = String::from("*");
                content.push_str(&counter.to_string());
                content.push_str("* - ");
                content.push_str(&title);

                state
                    .http
                    .create_message(msg.channel_id)
                    .content(&content)?
                    .await?;
            }
        }
    }

    Ok(())
}

async fn add(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
    queue: Arc<Mutex<Queue1>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let guild_id = msg.guild_id.unwrap();

        let b = msg.content.clone();
        let index1 = b.find(" ");
        let mut text: String;
        if !index1.is_none() {
            text = b.chars().skip(index1.unwrap()).collect();
        } else {
            text = "".to_string();
        }
        text = text.trim().replace(" ", "+");

        if let Some(_call_lock) = state.songbird.get(guild_id.into_nonzero()) {
            // let mut call = call_lock.lock().await;
            // let queue = queue.queues.entry(songbird::id::GuildId(guild_id.get()))

            // let queue_q = queue.lock().unwrap().queues.entry(songbird::id::GuildId(guild_id.get())).or_default();

            println!("{:?}", text);
            /*let source = ytdl(text)
            .await
              .expect("This might fail: handle this error!");*/

            let mut source = YoutubeDl::new(reqwest::Client::new(), text);

            //   let title =  source.metadata.title.as_ref().unwrap().clone();
            let mut title = "".to_string();
            if let Ok(metadata) = source.aux_metadata().await {
                let content = format!(
                    "**{:?}** added !",
                    metadata.title.as_ref().unwrap_or(&"<UNKNOWN>".to_string()),
                );
                title = content.clone();
            }

            // Queueing a track is this easy!
            //let hnd = queue.add_source(source.into(), &mut call);
            queue.lock().await.queue.push(source);

            state
                .http
                .create_message(msg.channel_id)
                .content(&title)?
                .await?;
        }
    }

    Ok(())
}

async fn description(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let song_link = state_info.lock().await.current_song_link.clone();

    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let guild_id = msg.guild_id.unwrap();
        let store = state.trackdata.read().await;
        if let Some(handle) = store.get(&guild_id) {
            let h = handle.get_info().await;
            if h.is_ok() {
                let content = reqwest::get(&song_link).await?.text().await?;

                let yt_struct = &yt_utils::get_link_content(content.as_str(), song_link.clone());

                state_info
                    .lock()
                    .await
                    .set_current_song_desc(yt_struct.get_yt_desc());

                let to_split = yt_struct.get_yt_desc();
                let re = Regex::new(r"\\n").unwrap();
                let result = re.replace_all(&to_split, "\n");
                let re2 = Regex::new(r"(https://)|(http://)").unwrap();
                let result2 = re2.replace_all(&result, "[http][//]");

                let re3 = Regex::new(r"\n\n").unwrap();
                let result3 = re3.replace_all(&result2, "\n");
                let result_final: String = result3.chars().take(1999).collect();
                state
                    .http
                    .create_message(msg.channel_id)
                    .content(&result_final)?
                    .await?;
            } else {
                state
                    .http
                    .create_message(msg.channel_id)
                    .content("`No song is currently playing!`")?
                    .await?;
            }
        }
    }

    Ok(())
}

async fn radiozu(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-i")
            .arg("https://ivm.antenaplay.ro/liveaudio/radiozu/playlist.m3u8")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();
        let guild_id = msg.guild_id.unwrap();
        let source = ImageSource::url(
            "https://static.tuneyou.com/images/logos/500_500/33/3133/RadioZU.jpg",
        )?;

        let embed = EmbedBuilder::new()
            .title("RadioZU Romania")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);

            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "RadioZU Romania".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }
    }

    Ok(())
}
async fn radio24house(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-i")
            .arg("https://24houseradio-adradio.radioca.st/128")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();
        let guild_id = msg.guild_id.unwrap();
        let source = ImageSource::url(
            "https://cdn2.vectorstock.com/i/1000x1000/01/16/radio-music-neon-logo-night-neon-vector-21420116.jpg",
        )?;

        let embed = EmbedBuilder::new()
            .title("Radio 24 House")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);

            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "Radio 24 House".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }
    }

    Ok(())
}

async fn radiouv(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-i")
            .arg("https://stream-21.zeno.fm/s98kga59qnruv?zs=y9vZRej5RV69g4Ld8fD7QQ")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();
        let guild_id = msg.guild_id.unwrap();
        let source = ImageSource::url(
            "https://cdn2.vectorstock.com/i/1000x1000/01/16/radio-music-neon-logo-night-neon-vector-21420116.jpg",
        )?;

        let embed = EmbedBuilder::new()
            .title("Radio Underground Vibe")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);
            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "Radio Underground Vibe".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }
    }

    Ok(())
}

async fn radiodancefmro(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-i")
            .arg("https://edge126.rcs-rds.ro/profm/dancefm.mp3")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();
        let guild_id = msg.guild_id.unwrap();
        let source = ImageSource::url(
            "https://cdn2.vectorstock.com/i/1000x1000/01/16/radio-music-neon-logo-night-neon-vector-21420116.jpg",
        )?;

        let embed = EmbedBuilder::new()
            .title("DanceFM.RO")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);

            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "DanceFM.ro".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }
    }

    Ok(())
}

async fn radiohouse(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-thread_queue_size")
            .arg("3")
            .arg("-i")
            .arg("https://deephouseradio.radioca.st/deep?type=http&nocache=16")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();
        let guild_id = msg.guild_id.unwrap();
        let source = ImageSource::url(
            "https://cdn2.vectorstock.com/i/1000x1000/01/16/radio-music-neon-logo-night-neon-vector-21420116.jpg",
        )?;

        let embed = EmbedBuilder::new()
            .title("Radio House Santa Monica")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);
            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "Radio House Santa monica".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }
    }

    Ok(())
}

async fn radioroman(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }
    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-thread_queue_size")
            .arg("3")
            .arg("-i")
            .arg("https://live.romanfm.ro:8000/;rfm")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();
        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();
        let guild_id = msg.guild_id.unwrap();
        let source = ImageSource::url(
            "https://cdn2.vectorstock.com/i/1000x1000/01/16/radio-music-neon-logo-night-neon-vector-21420116.jpg",
        )?;

        let embed = EmbedBuilder::new()
            .title("Radio Roman")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;
        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);
            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "Radio Roman".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }
    }

    Ok(())
}

async fn radiovirgin(
    msg: Message,
    state: State,
    state_info: Arc<Mutex<StateInfo>>,
) -> Result<(), Box<dyn Error + Send + Sync + 'static>> {
    let now = Instant::now();
    if !state_info.lock().await.is_joined {
        let res = join(msg.clone(), state.clone(), state_info.clone())
            .await
            .ok();

        match res {
            Some(result) => println!("{:?}", result),
            None => println!("ERR"),
        }
    }

    if state_info.lock().await.is_joined {
        let a = Command::new("ffmpeg")
            .arg("-i")
            .arg("https://astreaming.edi.ro:8443/VirginRadio_aac")
            .arg("-f")
            .arg("wav")
            .arg("-ac")
            .arg("2")
            .arg("-acodec")
            .arg("pcm_s16le")
            .arg("-ar")
            .arg("48000")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn();

        if !state_info.lock().await.ffmpeg_id.is_empty() {
            let id = state_info.lock().await.ffmpeg_id.pop().unwrap();

            let sp = Command::new("kill").arg(id.to_string()).output();
            let _res = sp.unwrap();
        }

        let ch = a.unwrap();
        let id = &ch.id();
        state_info.lock().await.set_ffmpeg_id(*id);

        let test = ChildContainer::from(ch).into();

        let guild_id = msg.guild_id.unwrap();

        let source = ImageSource::url(
            "https://virginradio.ro/wp-content/uploads/2019/06/VR_ROMANIA_WHITE-STAR-LOGO_RGB_ONLINE_1600x1600.png",
        )?;
        let embed = EmbedBuilder::new()
            .title("Virgin Radio Romania")
            .field(EmbedFieldBuilder::new("Requestor", msg.author.name).inline())
            .image(source)
            .validate()?
            .build();

        state
            .http
            .create_message(msg.channel_id)
            .embeds(&[embed])?
            .await?;

        if let Some(call_lock) = state.songbird.get(guild_id) {
            if state_info.lock().await.is_playing {
                let mut call = call_lock.lock().await;
                let _ = call.stop();
                state_info.lock().await.set_is_playing(false);
            }

            let mut call = call_lock.lock().await;
            let handle = call.play_input(test);
            state_info.lock().await.set_is_playing(true);

            let activity = Activity::from(MinimalActivity {
                kind: ActivityType::Listening,
                name: "Virgin Radio Romania".to_owned(),
                url: None,
            });
            let request = UpdatePresence::new(Vec::from([activity]), false, None, Status::Online)?;

            let _res = state.sender.get(state.shardid).unwrap().command(&request);

            let mut store = state.trackdata.write().await;
            store.insert(guild_id, handle);
        }

        let elapsed = now.elapsed();
        println!("Elapsed Radio: {:.2?}", elapsed);
    }

    Ok(())
}

fn get_discord_token() -> String {
    let mut return_string: String = String::default();
    let exist = Path::new("./token.txt").exists();
    if exist {
        let path = fs::canonicalize("./token.txt").unwrap();
        return_string = read_to_string(path)
            .expect("Unable to open file")
            .trim()
            .to_string();
    } else {
        let mut file1 = OpenOptions::new()
            .create(true)
            .write(true)
            .append(true)
            .read(true)
            .open("./token.txt")
            .unwrap();
        file1
            .write_all("<insert discord token here>".as_bytes())
            .expect("err");
    }
    return_string
}
